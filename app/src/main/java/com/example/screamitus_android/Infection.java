package com.example.screamitus_android;

public class Infection {

    public int calculateTotalInfected(int day)
    {
        int i;
        int infected = 0;
        // R1 : number of days must be > 0
        if (day <= 0)
        {
            return -1;
        }

        // R2 : the rate of infection is 5 instructors a day
        // R3 : the rate of infection after 7th day is 8 per instructor
        // R4 : on even number days, the infection rate drops to 0
        else
        {

            for (i = 1; i <= day; i++)
            {
                if ((day) % 2 == 0)
                {
                    infected = 0;
                    break;
                }
                else if (i > 7 )
                {
                    infected = infected + 8;
                }
                else
                    {
                    infected = infected + 5;
                }
            }
            return infected;
        }
    }

}
